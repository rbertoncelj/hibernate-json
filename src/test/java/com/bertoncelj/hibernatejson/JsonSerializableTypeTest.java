/*
Copyright 2014 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.hibernatejson;

import com.bertoncelj.hibernatejson.entity.Bar;
import com.bertoncelj.hibernatejson.entity.Foo;
import com.bertoncelj.hibernatejson.utils.TestPersistenceContext;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Rok Bertoncelj
 */
public class JsonSerializableTypeTest {
    private EntityManager em;
    private EntityManagerFactory emf;
    private TestPersistenceContext tpc;

    @BeforeMethod
    public void setUp() throws Exception {
        tpc = TestPersistenceContext.createRandom();
        em = tpc.getEntityManager();
        emf = tpc.getEntityManagerFactory();
    }

    @Test
    public void testPersistAndUpdate() throws Exception {
        Foo originalFoo = new Foo();
        originalFoo.setName("test123");

        originalFoo.setBar(new Bar("bar1", 1, true));

        originalFoo.setBars(new ArrayList<Bar>(2));
        originalFoo.getBars().add(new Bar("bar2", 2, true));
        originalFoo.getBars().add(new Bar("bar3", 3, false));

        originalFoo.setBarMap(new HashMap<String, Bar>());
        originalFoo.getBarMap().put("A", new Bar("barA", 4, true));
        originalFoo.getBarMap().put("B", new Bar("barB", 5, false));

        em.getTransaction().begin();
        em.persist(originalFoo);
        commitAndReset();

        em.getTransaction().begin();
        Foo loadedFoo = em.createQuery("select f from Foo f", Foo.class).getSingleResult();

        Assert.assertEquals(loadedFoo, originalFoo);
        Assert.assertNotSame(loadedFoo, originalFoo);

        loadedFoo.getBar().setId("foobar-id");
        commitAndReset();

        em.getTransaction().begin();
        Foo reloadedUpdatedFoo = em.createQuery("select f from Foo f", Foo.class).getSingleResult();

        Assert.assertNotEquals(reloadedUpdatedFoo, originalFoo);
        Assert.assertEquals(reloadedUpdatedFoo, loadedFoo);
        Assert.assertNotSame(reloadedUpdatedFoo, loadedFoo);
    }

    private void commitAndReset() {
        em.flush();
        em.getTransaction().commit();
        em = emf.createEntityManager();
    }
}