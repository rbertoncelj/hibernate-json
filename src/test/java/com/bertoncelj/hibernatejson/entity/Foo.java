/*
Copyright 2014 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.hibernatejson.entity;

import com.bertoncelj.hibernatejson.JsonSerializableType;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.Map;

/**
 * @author Rok Bertoncelj
 */
@Entity
public class Foo {
    @Id
    private String name;

    @Type(type = JsonSerializableType.CLASS_NAME, parameters = {
            @Parameter(name = JsonSerializableType.OBJECT_TYPE, value = "com.bertoncelj.hibernatejson.entity.Bar")})
    private Bar bar;

    @Type(type = JsonSerializableType.CLASS_NAME, parameters = {
            @Parameter(name = JsonSerializableType.LIST_TYPE, value = "com.bertoncelj.hibernatejson.entity.Bar")})
    private List<Bar> bars;

    @Type(type = JsonSerializableType.CLASS_NAME, parameters = {
            @Parameter(name = JsonSerializableType.MAP_KEY_TYPE, value = "java.lang.String"),
            @Parameter(name = JsonSerializableType.MAP_VALUE_TYPE, value = "com.bertoncelj.hibernatejson.entity.Bar")})
    private Map<String, Bar> barMap;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bar getBar() {
        return bar;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    public List<Bar> getBars() {
        return bars;
    }

    public void setBars(List<Bar> bars) {
        this.bars = bars;
    }

    public Map<String, Bar> getBarMap() {
        return barMap;
    }

    public void setBarMap(Map<String, Bar> barMap) {
        this.barMap = barMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Foo foo = (Foo) o;

        if (bar != null ? !bar.equals(foo.bar) : foo.bar != null) return false;
        if (barMap != null ? !barMap.equals(foo.barMap) : foo.barMap != null) return false;
        if (bars != null ? !bars.equals(foo.bars) : foo.bars != null) return false;
        if (name != null ? !name.equals(foo.name) : foo.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (bar != null ? bar.hashCode() : 0);
        result = 31 * result + (bars != null ? bars.hashCode() : 0);
        result = 31 * result + (barMap != null ? barMap.hashCode() : 0);
        return result;
    }
}
