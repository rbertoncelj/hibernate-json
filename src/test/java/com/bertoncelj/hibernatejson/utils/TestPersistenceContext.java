/*
Copyright 2014 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.hibernatejson.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Rok Bertoncelj
 */
public class TestPersistenceContext {
    public static final String TEST_PERSISTENCE_UNIT_NAME = "test-persistence-unit";

    private static TestPersistenceContext defaultInstance =
            new TestPersistenceContext("default-test-db", TEST_PERSISTENCE_UNIT_NAME);
    private static Random rnd = new Random();

    private String databaseName;
    private String persistenceUnitName;

    private EntityManagerFactory emf;
    private EntityManager em;

    public TestPersistenceContext(String databaseName, String persistenceUnitName) {
        this.databaseName = databaseName;
        this.persistenceUnitName = persistenceUnitName;
    }

    public static TestPersistenceContext createRandom() {
        return new TestPersistenceContext("test-db-" + System.currentTimeMillis() + "-" + rnd.nextInt(1000000), TEST_PERSISTENCE_UNIT_NAME);
    }

    public static TestPersistenceContext getDefault() {
        return defaultInstance;
    }

    public EntityManager getEntityManager() {
        if (em == null) {
            init();
        }
        if (!em.isOpen()) {
            em = emf.createEntityManager();
        }
        return em;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            init();
        }
        return emf;
    }

    public void resetJpa() {
        if (em != null) {
            if (em.isOpen()) {
                em.close();
            }
            em = null;
        }
        if (emf != null) {
            if (emf.isOpen()) {
                emf.close();
            }
            emf = null;
        }
    }

    private void init() {
        Map<String, Object> properties = new HashMap<String, Object>();

        properties.put("hibernate.connection.driver_class", "org.h2.Driver");
        properties.put("hibernate.connection.username", "sa");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.connection.url", "jdbc:h2:mem:" + databaseName + ";DB_CLOSE_DELAY=-1");
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.auto", "create-drop");

        emf = Persistence.createEntityManagerFactory(persistenceUnitName, properties);
        em = emf.createEntityManager();
    }

    public Connection getDatabaseConnection() {
        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection("jdbc:h2:mem:" + databaseName + ";DB_CLOSE_DELAY=-1", "sa", "");
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
