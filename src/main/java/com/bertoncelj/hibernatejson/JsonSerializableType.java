/*
Copyright 2014 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.hibernatejson;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Rok Bertoncelj
 */
public class JsonSerializableType implements UserType, ParameterizedType {
    public static final String CLASS_NAME = "com.bertoncelj.hibernatejson.JsonSerializableType";
    public static final String OBJECT_TYPE = "objectType";
    public static final String LIST_TYPE = "listType";
    public static final String MAP_KEY_TYPE = "mapKeyType";
    public static final String MAP_VALUE_TYPE = "mapValueType";

    private static final int[] SQL_TYPES = {Types.CLOB};
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Properties parameters;
    private JavaType javaType;

    @Override
    public void setParameterValues(Properties parameters) {
        this.parameters = parameters;

        Class<?> objectType = readType(OBJECT_TYPE);
        if (objectType != null) {
            javaType = MAPPER.getTypeFactory().constructType(objectType);
            return;
        }

        Class<?> listType = readType(LIST_TYPE);
        if (listType != null) {
            javaType = MAPPER.getTypeFactory().constructCollectionType(List.class, listType);
            return;
        }

        Class<?> mapKeyType = readType(MAP_KEY_TYPE);
        Class<?> mapValueType = readType(MAP_VALUE_TYPE);
        if (mapKeyType != null && mapValueType != null) {
            javaType = MAPPER.getTypeFactory().constructMapType(Map.class, mapKeyType, mapValueType);
            return;
        }

        throw new HibernateException("Could not determine the type of object that needs to be stored as JSON.");
    }

    private Class<?> readType(String paramName) {
        String paramValue = parameters.getProperty(paramName);
        try {
            return paramValue != null ? Class.forName(paramValue) : null;
        } catch (ClassNotFoundException e) {
            throw new HibernateException("Could not determine type of '" + paramName + "'.", e);
        }
    }

    @Override
    public int[] sqlTypes() {
        return SQL_TYPES;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
        return javaType.getRawClass();
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return Objects.equal(toJson(x), toJson(y));
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return Objects.hashCode(toJson(x));
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws SQLException {
        String value = rs.getString(names[0]);
        return fromJson(value);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws SQLException {
        st.setString(index, toJson(value));
    }

    @Override
    public Object deepCopy(Object value) {
        return value != null
                ? fromJson(toJson(value))
                : null;
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(Object value) {
        return toJson(value);
    }

    @Override
    public Object assemble(Serializable cached, Object owner) {
        return fromJson((String) cached);
    }

    @Override
    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return this.deepCopy(original);
    }

    protected String toJson(Object object) {
        try {
            return MAPPER.writeValueAsString(object);
        } catch (IOException e) {
            throw new HibernateException(e);
        }
    }

    protected Object fromJson(String json) {
        try {
            return json != null ? MAPPER.readValue(json, javaType) : null;
        } catch (IOException e) {
            throw new HibernateException(e);
        }
    }

}
